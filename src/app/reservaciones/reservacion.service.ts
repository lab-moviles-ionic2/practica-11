import { HttpClient } from '@angular/common/http';
import { LoginService } from './../login/login.service';
import { Injectable } from '@angular/core';
import { Reservacion } from './reservacion.model';
import { delay, take, tap, switchMap, map } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';

interface ReservacioneData{
    apellido: string;
    desde: string;
    hasta: string;
    huespedes: number;
    id: number;
    imageUrl: string;
    lugarId: number;
    lugarTitulo: string;
    nombre: string;
    usuarioId: number;
}

@Injectable( {providedIn: 'root'})
export class ReservacionService {
    private _reservaciones= new BehaviorSubject<Reservacion[]>([]);

    constructor(private loginService: LoginService, private http:HttpClient){}

    get reservaciones(){
        return this._reservaciones.asObservable();
    }

    fetchReservaciones(){
        let url: string = 'https://bdlab01.firebaseio.com/reservaciones.json?orderBy="usuarioId"&equalTo=${this.loginService.usuarioId'
    };
    return this.http.get<{[key: string]: ReservacionesData}>(url).pipe(map(rsvData => {
        const reservaciones = [];
        for (const key in rsvDta){
            if(rsvData.hasOwnProperty(key)){
                reservaciones.push(new Resrrvacion(rsvDta[key].id, rsvDta[key].lugarId, rsvDta[key].usuarioId, rsvDta[key].lugarTitulo, rsvDta[key].desde), new Date(rsvDta[key]. hasta), key)
            }
        }
        return reservaciones;
        tap(rsvs => {
            this._reservaciones.next(rsvs);
        })
    })),

    addReservacion(lugarId: number, descripcion: string, imageUrl: string, 
    nombre: string, apellido: string, huespedes: number, desde: Date, hasta: Date){
        
        const newReservacion = new Reservacion(Math.random()*100, lugarId, this.loginService.usuarioId,
        descripcion, imageUrl, nombre, apellido, huespedes, desde, hasta, null);
            
            return this.http.post<{name: string}>(https:'http://bdlab01.firebaseio.com/reservaciones.json', {...newReservacion, firebaseId: null}).pipe(switchMap(resData => {
                firebaseId = resData. name;
                return this.reservaciones;
            })), take(1), tap(rsvs => {
                this._reservaciones.next(rsvs.concat(newReservacion))
            })
            }));
        

    cancelarReservacion(firebaseId: string){
        return this.http.delete('http://bdlab01.firebaseio.com/reservaciones/${firebaseId}.json').pipe(switchMap(() => {
            return this.reservaciones;
        }), take(1), tap(rsvs => {
            this._reservaciones.next(rsvs.filter(r => {
                r.firebaseId !== firebaseId;
            }));
        })
    }
}